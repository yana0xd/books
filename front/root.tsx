import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import Home from '~/pages/Home'
import Cart from '~/pages/Cart'
import Order from '~/pages/Order'

import { Provider } from 'react-redux'
import { store } from '~/store'

export default function Root() {
  return (
    <React.StrictMode>
      <Provider store={store}>
        <Router>
          <div>
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route path="/cart">
                <Cart />
              </Route>
              <Route path="/order">
                <Order />
              </Route>
            </Switch>
          </div>
        </Router>
      </Provider>
    </React.StrictMode>
  )
}
