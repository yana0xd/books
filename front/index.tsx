import React from 'react'
import ReactDOM from 'react-dom'
import './assets/scss/index.scss'
import Root from './root'

ReactDOM.render(<Root />, document.getElementById('root'))
