import React, { useState } from 'react'
import { Button } from '~/components/ui'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import styles from './Book.module.scss'

type BookProps = {
  book: Book
  onAdd?: (book: Book) => void
  onDelete?: (book: Book) => void
  onSetQuantity?: (book: Book, quantity: number) => void
}

export default function Book({
  book,
  onAdd,
  onDelete,
  onSetQuantity
}: BookProps) {
  const [quantity, setQuantity] = useState<number>(book?.quantity || 0)
  const isDisabled = (): boolean => {
    return quantity <= 0
  }

  const isDisabledAdd = () => {
    return typeof book?.quantity === 'number' ? book?.quantity <= 0 : false
  }

  const price = `${book.price} ${book.currency}`
  const pages = `${book.pages} pages`

  const onChangeQuantity = (editedBook: Book, editedQuantity: number) => {
    setQuantity(editedQuantity)
    if (onSetQuantity) {
      onSetQuantity(editedBook, editedQuantity)
    }
  }

  return (
    <div className={styles.book}>
      {!!onDelete && (
        <button className={styles.book__delete} onClick={() => onDelete(book)}>
          <p className={styles['book__delete-title']}>Delete</p>
          <FontAwesomeIcon icon={faTrash} />
        </button>
      )}
      <div className={styles.book__container}>
        <img alt="cover" src={book.cover_url} />
      </div>
      <div className={styles.book__author}>{book.author}</div>

      <div className={styles.book__title}>{book.title}</div>

      <div className={styles.book__price}>
        <p>{price}</p>
        <p>{pages}</p>
      </div>

      {!!onAdd && (
        <div className={styles.book__add}>
          <div className={styles.book__quantity}>
            <button
              disabled={isDisabled()}
              onClick={() => onChangeQuantity(book, quantity - 1)}
            >
              -
            </button>
            <div className={styles.book__count}>{quantity}</div>

            <button onClick={() => onChangeQuantity(book, quantity + 1)}>
              +
            </button>
          </div>

          <Button disabled={isDisabledAdd()} onClick={() => onAdd(book)}>
            Add to cart
          </Button>
        </div>
      )}
    </div>
  )
}
