import React, { useEffect, useState, useCallback } from 'react'
import { getBooks } from '~/api/books'
import Book from '~/components/Book'
import { Dispatch } from 'redux'
import { useDispatch, connect } from 'react-redux'
import { addToCart } from '~/store/actions'

import * as Yup from 'yup'
import { Formik, Form, FormikHelpers } from 'formik'
import { Input, Pagination, Button } from '~/components/ui'
import styles from './List.module.scss'

function List() {
  const dispatch: Dispatch<Action> = useDispatch()

  const add = useCallback((book: Book) => dispatch(addToCart(book)), [dispatch])

  const validation = Yup.object().shape({
    title: Yup.string(),
    author: Yup.string()
  })

  const initialValues = {
    title: '',
    author: ''
  }

  const [books, setBooks] = useState<Book[] | []>([])

  const [searchParams, setSearchParams] = useState<SearchParams>({
    page: 1,
    title: '',
    author: ''
  })

  const [meta, setMeta] = useState<Meta>({
    page: 1,
    records_per_page: 1,
    total_records: 1
  })

  const setQuantity = (book: Book, quantity: number) => {
    const index: number = books.findIndex((item: Book) => {
      return item.id === book.id
    })
    if (index !== -1) {
      const filteredBooks = books.filter((item: Book, ind: number) => {
        return ind !== index
      })
      const updatedBook = { ...book, quantity }

      setBooks([...filteredBooks, updatedBook])
    }
  }

  const [isLoading, setIsLoading] = useState<boolean>(false)

  const fetchBooks = useCallback(async () => {
    setIsLoading(true)

    const data = (await getBooks(searchParams)) as ApiResponse<Book[]>
    setBooks(data.data)
    if (data.metadata) {
      setMeta(data.metadata)
    }

    setIsLoading(false)
  }, [searchParams])

  useEffect(() => {
    fetchBooks().catch((error) => console.log(error))
  }, [fetchBooks])

  const setPage = (page: number) => {
    setSearchParams({ ...searchParams, page })
  }

  const search = (values: Search) => {
    setSearchParams({
      ...searchParams,
      title: values.title,
      author: values.author
    })
  }

  return (
    <div className={styles.list}>
      <Formik
        validationSchema={validation}
        initialValues={initialValues}
        onSubmit={(values) => search(values)}
      >
        {({ values, handleChange }) => {
          return (
            <Form className={styles.list__search}>
              <Input
                label="Title"
                id="title"
                value={values.title}
                onChange={handleChange}
              />
              <Input
                label="Author"
                id="author"
                value={values.author}
                onChange={handleChange}
              />

              <Button
                className={styles.list__button}
                onClick={() => search(values)}
                type="submit"
              >
                Search
              </Button>
            </Form>
          )
        }}
      </Formik>
      <div className={styles.list__content}>
        {!isLoading &&
          !!books.length &&
          books.map((book: Book) => {
            return (
              <Book
                onSetQuantity={setQuantity}
                onAdd={() => add(book)}
                book={book}
                key={book.id}
              />
            )
          })}
      </div>

      <Pagination
        title={searchParams.title}
        author={searchParams.author}
        onSetPage={setPage}
        className={styles.list__pagination}
        total={meta?.total_records}
        perPage={meta.records_per_page}
      />
    </div>
  )
}

const mapStateToProps = (state: State) => ({
  books: state.books
})

export default connect(mapStateToProps)(List)
