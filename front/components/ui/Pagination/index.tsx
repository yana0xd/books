import React, { useState, useEffect } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons'
import classnames from 'classnames'
import styles from './Pagination.module.scss'

type PaginationProps = {
  total: number
  perPage: number
  className?: string
  onSetPage: (page: number) => void
  title: any
  author: any
}

export function Pagination({
  total,
  perPage = 10,
  className,
  onSetPage,
  title,
  author
}: PaginationProps) {
  const [totalPages, setTotalPages] = useState<number>(1)
  const [currentPage, setCurrentPage] = useState<number>(1)

  useEffect(() => {
    const pages = Math.ceil(total / perPage)
    setTotalPages(pages)
  }, [total, perPage, totalPages])

  useEffect(() => {
    setCurrentPage(currentPage)
    onSetPage(currentPage)
  }, [currentPage])

  useEffect(() => {
    setCurrentPage(1)
    onSetPage(1)
  }, [title, author])

  const isFirstPage = () => {
    return currentPage === 1
  }

  const isLastPage = () => {
    return currentPage === totalPages || totalPages === 0
  }

  const back = (page: number): void => {
    setCurrentPage(page)
  }

  const next = (page: number): void => {
    setCurrentPage(page)
  }

  return (
    <div className={classnames(styles.pagination, className)}>
      <button disabled={isFirstPage()} onClick={() => back(currentPage - 1)}>
        <FontAwesomeIcon icon={faAngleLeft} />
      </button>
      <div className={styles.pagination__page}>{currentPage}</div>
      <button disabled={isLastPage()} onClick={() => next(currentPage + 1)}>
        <FontAwesomeIcon icon={faAngleRight} />
      </button>
    </div>
  )
}
