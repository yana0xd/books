import React from 'react'
import classnames from 'classnames'
import styles from './Input.module.scss'

type InputProps = {
  value: string | number
  label?: string
  error?: string
} & React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>

export function Input({ value, type, onChange, id, label, error }: InputProps) {
  return (
    <div className={classnames(styles.input)}>
      <label className={styles.input__label}>{label}</label>
      <input
        className={styles.input__field}
        onChange={onChange}
        value={value}
        type={type}
        id={id}
      />
      {error && <div className={styles.input__error}>{error}</div>}
    </div>
  )
}
