import React from 'react'
import classnames from 'classnames'
import styles from './Button.module.scss'

type ButtonProps = {
  children: React.ReactNode
  className?: string
} & React.DetailedHTMLProps<
  React.ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
>

export function Button({
  children,
  disabled,
  className,
  onClick,
  type
}: ButtonProps) {
  return (
    <button
      disabled={disabled}
      onClick={onClick}
      type={type}
      className={classnames(styles.button, className)}
    >
      {children}
    </button>
  )
}
