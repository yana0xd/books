import React, { useCallback } from 'react'
import { connect, useDispatch } from 'react-redux'
import Book from '~/components/Book'
import { Button } from '~/components/ui'
import { Link } from 'react-router-dom'
import { Dispatch } from 'redux'
import { removeFromCart } from '~/store/actions'
import styles from './Cart.module.scss'

function Cart({ books }: { books: Book[] }) {
  const dispatch: Dispatch<Action> = useDispatch()

  const remove = useCallback(
    (book: Book) => dispatch(removeFromCart(book)),
    [dispatch]
  )

  const hasBooks = !!books.length

  const selected = (): React.ReactNode => {
    if (hasBooks) {
      return (
        <div className={styles.cart__content}>
          {books.map((book: Book) => {
            return (
              <Book onDelete={() => remove(book)} key={book.id} book={book} />
            )
          })}
        </div>
      )
    }

    return <>Your shopping cart is empty</>
  }
  return (
    <div className="container">
      <div className={styles.cart__header}>
        <div className={styles.cart__title}>Cart</div>
      </div>
      {selected()}
      {hasBooks && (
        <div>
          <Button>
            <Link to="/order">Next</Link>
          </Button>
        </div>
      )}
    </div>
  )
}

const mapStateToProps = (state: State) => ({
  books: state.books
})

export default connect(mapStateToProps)(Cart)
