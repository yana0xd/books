import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import List from '~/components/List'
import { Link } from 'react-router-dom'
import styles from './Home.module.scss'

export default function Home() {
  return (
    <div className="container">
      <div className={styles.home__header}>
        <div className={styles.home__title}>List of available books</div>
        <div className={styles.home__cart}>
          <FontAwesomeIcon
            className={styles.home__icon}
            icon={faShoppingCart}
          />

          <Link to="/cart">Cart</Link>
        </div>
      </div>
      <List />
    </div>
  )
}
