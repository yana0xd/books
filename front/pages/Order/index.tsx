import React, { useCallback } from 'react'
import { Formik, Form, FormikErrors, FormikTouched } from 'formik'
import * as Yup from 'yup'
import { Button, Input } from '~/components/ui'
import { connect, useDispatch } from 'react-redux'
import { createOrder } from '~/api/order'
import { Dispatch } from 'redux'
import { clearCart } from '~/store/actions'

import styles from './Order.module.scss'

function Order({ books }: { books: Book[] | [] }) {
  const dispatch: Dispatch<Action> = useDispatch()

  const clear = useCallback(
    (ids: { id: number }[]) => dispatch(clearCart(ids)),
    [dispatch]
  )

  const initialValues = {
    first_name: '',
    last_name: '',
    city: '',
    zip_code: ''
  }

  const validation = Yup.object().shape({
    first_name: Yup.string().required().min(2),
    last_name: Yup.string().required().min(5),
    city: Yup.string().required(),
    zip_code: Yup.string()
      .required()
      .matches(/\d{2}-\d{3}/)
  })

  const submit = (
    values: User,
    setSubmitting: (isSubmitting: boolean) => void
  ): void => {
    const orderedBooks: Order[] = books.map((book: Book) => {
      return { quantity: book.quantity || 1, id: book.id }
    })

    createOrder(values, orderedBooks)
      // clear cart if all ok
      .then(({ data }: { data: FormedOrder }) => {
        const ordered = data.order.map((item: Order) => {
          return { id: item.id }
        })

        clear(ordered)
      })
      .catch((e) => console.log(e))

    setSubmitting(false)
  }

  const isInvalid = (
    touched: FormikTouched<User>,
    errors: FormikErrors<User>
  ): boolean => {
    const isTouched = !!Object.keys(touched).length
    const isError = !!Object.keys(errors).length

    return isTouched && isError
  }

  return (
    <div className="container">
      <div className={styles.order__header}>
        <div className={styles.order__title}>Order</div>
      </div>
      <Formik
        initialValues={initialValues}
        validationSchema={validation}
        onSubmit={(values, formik) => submit(values, formik.setSubmitting)}
      >
        {({ values, handleChange, errors, isSubmitting, touched }) => {
          return (
            <Form className={styles.order__form}>
              <Input
                error={
                  touched.first_name && errors.first_name
                    ? errors.first_name
                    : ''
                }
                id="first_name"
                label="First name"
                value={values.first_name}
                onChange={handleChange}
              />

              <Input
                error={
                  touched.last_name && errors.last_name ? errors.last_name : ''
                }
                id="last_name"
                label="Last name"
                value={values.last_name}
                onChange={handleChange}
              />

              <Input
                error={touched.city && errors.city ? errors.city : ''}
                id="city"
                label="City"
                value={values.city}
                onChange={handleChange}
              />

              <Input
                error={
                  touched.zip_code && errors.zip_code ? errors.zip_code : ''
                }
                id="zip_code"
                label="Zip code"
                value={values.zip_code}
                onChange={handleChange}
              />

              <Button
                type="submit"
                disabled={isSubmitting || isInvalid(touched, errors)}
              >
                Create order
              </Button>
            </Form>
          )
        }}
      </Formik>
    </div>
  )
}

const mapStateToProps = (state: State) => ({
  books: state.books
})

export default connect(mapStateToProps)(Order)
