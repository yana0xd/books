import axios, { AxiosError } from 'axios'
import { instance } from './index'

type BookParams = {
  page: number
  title?: string
  author?: string
}

export const getBooks = async ({
  page,
  title = '',
  author = ''
}: BookParams): Promise<GetBooksResponse | any> => {
  try {
    const { data } = await instance.get(
      `/book?page=${page}&search[title]=${title}&search[author]=${author}`
    )
    const res = data as ApiResponse<Book[]>

    const books: Book[] = res.data.map((book: Book) => {
      const url = new URL(book.cover_url)
      const { pathname } = url
      return {
        ...book,
        quantity: 0,
        cover_url: `http://localhost:3001${pathname}`
      }
    })
    return { data: books, metadata: res.metadata }
  } catch (error: any) {
    if (axios.isAxiosError(error)) {
      const e = error as AxiosError
      return e.response?.data
    }
  }
}
