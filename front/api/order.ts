import axios, { AxiosError } from 'axios'
import { instance } from './index'

export const createOrder = async (
  user: User,
  orders: Order[]
): Promise<CreateOrderResponse | any> => {
  try {
    const formedorder: FormedOrder = { ...user, order: orders }
    const { data } = await instance.post('/order', formedorder)
    const response = data as CreateOrderResponse
    return response
  } catch (error) {
    if (axios.isAxiosError(error)) {
      const e = error as AxiosError
      return e.response?.data
    }
  }
}
