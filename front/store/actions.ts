import * as actionTypes from './actionTypes'

export function addToCart(book: Book) {
  const action = {
    type: actionTypes.ADD_TO_CART,
    book
  }
  return action
}

export function removeFromCart(book: Book) {
  const action = {
    type: actionTypes.REMOVE_FROM_CART,
    book
  }
  return action
}

export function clearCart(booksId: { id: number }[]) {
  const action = {
    type: actionTypes.CLEAR_CART,
    books: booksId
  }
  return action
}
