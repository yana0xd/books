import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import * as actionTypes from './actionTypes'

const initialState = {
  books: []
}

const reducer = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case actionTypes.ADD_TO_CART: {
      if (!action.book) {
        return state
      }
      const newBooks = [...state.books, action.book]
      const filteredBooks = newBooks.filter((item: Book, index: number) => {
        return newBooks.indexOf(item) === index
      })

      return { ...state, books: filteredBooks }
    }

    case actionTypes.REMOVE_FROM_CART: {
      if (!action.book) {
        return state
      }
      const removedId: number = action.book.id
      const filtered = state.books.filter((book: Book) => {
        return book.id !== removedId
      })

      return { ...state, books: filtered }
    }

    case actionTypes.CLEAR_CART: {
      if (!action.books) {
        return state
      }
      const ordered = action.books.map((item: { id: number }) => item.id)
      const filtered = state.books.filter((book: Book) => {
        return !ordered.includes(book.id)
      })

      return { ...state, books: filtered }
    }

    default:
      return state
  }
}

const store = createStore(reducer, applyMiddleware(createLogger()))

export { reducer, store }
