declare module '*.module.scss'

type ApiResponse<DataType = any, MetaType = Meta> = {
  data: DataType
  metadata?: MetaType
}

type GetBooksResponse = ApiResponse<Book[]>

type OrderResponse = {
  new_teacher: boolean
  ordered_materials: boolean
}

type CreateOrderResponse = ApiResponse<FormedOrder>

type Meta = {
  page: number
  records_per_page: number
  total_records: number
}

type Book = {
  author: string
  cover_url: string
  currency: string
  id: number
  pages: number
  price: number
  title: string
  quantity?: number
}

type SearchParams = {
  page: number
  title?: string
  author?: string
}

type Search = {
  title: string
  author: string
}

type User = {
  first_name: string
  last_name: string
  city: string
  zip_code: string
}

type Order = {
  id: number
  quantity: number
}

type FormedOrder = User & {
  order: Order[]
}

type State = {
  books: Book[] | []
}

type Action = {
  type: string
  book?: Book
  books?: { id: number }[]
}
