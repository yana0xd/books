const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const ESLintPlugin = require('eslint-webpack-plugin')
const { HotModuleReplacementPlugin } = require('webpack')
const CaseSensitivePlugin = require('case-sensitive-paths-webpack-plugin')
const Dotenv = require('dotenv-webpack')

module.exports = {
  mode: 'development',
  entry: './front/index.tsx',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, './front/dist'),
    publicPath: './'
  },
  stats: {
    children: true
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.scss'],
    alias: {
      '~': path.resolve(__dirname, 'front'),
      pages: path.resolve(__dirname, 'front/pages'),
      components: path.resolve(__dirname, 'front/components'),
      api: path.resolve(__dirname, 'front/api'),
      style: path.resolve(__dirname, 'front/assets/scss'),
      store: path.resolve(__dirname, 'front/store')
    }
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: '/node_modules/'
      },
      {
        test: /\.js|jsx$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.(s*)css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              esModule: false,
              modules: {
                localIdentName: '[local]'
              }
            }
          },

          'sass-loader',
          {
            loader: 'sass-resources-loader',
            options: {
              resources: path.resolve(__dirname, 'front/assets/scss/index.scss')
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/i,
        type: 'asset/resource'
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        exclude: /node_modules/,
        use: 'file-loader'
      }
    ]
  },
  plugins: [
    new Dotenv({ path: path.resolve(__dirname, '.env') }),
    new HtmlWebpackPlugin({
      template: 'index.html',
      appMountId: 'root',
      filename: 'index.html',
      inject: true
    }),
    new HotModuleReplacementPlugin(),
    new ESLintPlugin(),
    new MiniCssExtractPlugin({ filename: 'style.css' }),
    new CaseSensitivePlugin()
  ],
  devtool: 'source-map',
  devServer: {
    port: 9000,
    open: true,
    hot: true,
    historyApiFallback: true,
    proxy: {
      '/api': 'http://localhost:3001'
    }
  }
}
