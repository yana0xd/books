module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    "plugin:react/recommended",
    "airbnb",
    "plugin:import/recommended",
    "prettier",
  ],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: "module",
  },
  settings: {
    "import/resolver": {
      node: {
        extensions: [".ts", ".tsx", ".js", ".jsx", ".scss"],
      },

      alias: {
        map: [
          ["~", "./front"],
          ["pages", "./front/pages"],
          ["components", "./front/components"],
          ["api", "./front/api"],
          ["style", "./front/assets/scss"],
        ],
        extensions: [".ts", ".tsx", ".js", ".jsx", ".scss", ".png", ".jpg"],
      },
    },
  },
  // чтобы линтер не орал на JSX как тип возвращаемый функцией
  overrides: [
    {
      files: ["*.ts", "*.tsx"],
      rules: {
        "no-undef": "off",
      },
    },
  ],
  plugins: ["react", "@typescript-eslint", "prettier", "react-hooks"],
  rules: {
    "consistent-return": "off",
    "import/prefer-default-export": "off",
    "import/no-dynamic-require": "off",
    "no-plusplus": "off",
    "no-restricted-syntax": "off",
    "jsx-a11y/no-static-element-interactions": "off",
    "import/extensions": [
      "error",
      "ignorePackages",
      {
        js: "never",
        jsx: "never",
        ts: "never",
        tsx: "never",
      },
    ],
    "react/jsx-filename-extension": [
      2,
      { extensions: [".js", ".jsx", ".ts", ".tsx"] },
    ],
    "no-use-before-define": "off",
    "@typescript-eslint/no-use-before-define": ["error"],
    "prettier/prettier": ["warn"],
    "space-before-function-paren": "off",
    "react/prefer-stateless-function": "warn",
    "max-len": [
      "warn",
      {
        ignoreComments: true,
        ignoreStrings: true,
        code: 90,
      },
    ],
    "react/jsx-uses-react": 1,
    "react/button-has-type": "off",
    "react/default-props-match-prop-types": "off",
    "react/no-array-index-key": "off",
    "react/sort-comp": "off",
    "import/no-cycle": "off",
    "react/prop-types": "off",
    "linebreak-style": "off",
    "global-require": "off",
    semi: "off",
    "arrow-body-style": "off",
    "comma-dangle": "off",
    "class-methods-use-this": "off",
    "quote-props": "off",
    "react/destructuring-assignment": "off",
    "react/jsx-wrap-multilines": "off",
    "react/jsx-one-expression-per-line": "off",
    "react-hooks/rules-of-hooks": "warn",
    "react-hooks/exhaustive-deps": "warn",
    "jsx-a11y/label-has-associated-control": [
      "error",
      {
        required: {
          some: ["nesting", "id"],
        },
      },
    ],
    "jsx-a11y/no-autofocus": "warn",
    "jsx-a11y/no-noninteractive-element-interactions": "off",
    "jsx-a11y/click-events-have-key-events": "off",
    "max-classes-per-file": "warn",
    "react/jsx-props-no-spreading": ["off"],
    "react/require-default-props": ["off"],
    camelcase: "off",
    "no-unused-vars": "warn",
  },
};
